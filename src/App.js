import logo from './logo.svg';
import './App.css';
import Login from './Login'; 
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Regestration from './Regestration';

function App() {
  return (
    <div className="App">
      
      <Switch>
       <Route exact path="/" component={Login} />
       <Route path="/registration" component={Regestration}/>
      </Switch>        
     
    </div>
  );
}

export default App;
