import { useEffect, useState } from "react";
import {
  Link
} from "react-router-dom";

function Login(){

  useEffect(()=>{
   
   // alert("From Login useEffect")

  },[])

  const[greet,setGreet]=useState(false)
  const [uName, setUname] = useState("")
  const [password, setPassword] = useState("")

  const handleChange = e =>{
    setUname(e.target.value)
    
  }

  const pWordChange = f =>{

    setPassword(f.target.value)
  }
  const btnClick = () =>{

    alert("From button click");
    alert("Hello Mr." + uName);
  }

  const Greet =() =>{
     
    setGreet(true);

  }
  const Signup =()=>{

    alert("signup cliked")
  }
    return(
        <>
        {greet && <h2>Hello Mr. {uName}</h2> }
        <form>
        <label>
          User Name: <input type="text" value={uName} onChange={handleChange} /> <br/>
          Password: <input type="password" value={password} onChange={pWordChange}/> <br/>
           <input type="button" value="Login" onClick={Greet} />
           <input type="button" value="SignUp" onClick={Signup}></input>
           <Link to="/registration">SingUp</Link>

        </label>
      </form>
      
        </>
    )
}
export default Login;